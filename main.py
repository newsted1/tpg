from __future__ import division
from IPython.display import clear_output
import numpy as np
import matplotlib.pyplot as plt
from IPython import display
import time
import random
import pickle
import argparse
from dotenv import load_dotenv
load_dotenv()

import retro
import os

from tpg.tpg_trainer import TpgTrainer
from tpg.tpg_agent import TpgAgent

from utils.game_states import SonicAndKnucklesState
from utils.input_mappings import SonicDiscretizer

def show_state(env, step=0, name='', info=''):
    plt.figure(3)
    plt.clf()
    plt.imshow(env.render(mode='rgb_array'))
    plt.title("%s | Step: %d %s" % (name, step, info))
    plt.axis('off')

    display.clear_output(wait=True)
    display.display(plt.gcf())

def getState(inState):
    # Comes as a 2D matrix of colors. We want to reshape that into a 1D array with 3 colors each
    rgbRows = np.reshape(inState, (len(inState[0])*len(inState), 3)).T
    return np.add(np.left_shift(rgbRows[0], 16), np.add(np.left_shift(rgbRows[1], 8), rgbRows[2]))

'''
Available games: 
* MegaMan2-Nes
* SonicTheHedgehog3-Genesis
* SonicAndKnuckles3-Genesis
* SuperMarioWorld-Snes
* Vectorman-Genesis
'''
parser = argparse.ArgumentParser()
parser.add_argument('--game', default='SonicAndKnuckles3-Genesis', type=str, help='What game to train over')
parser.add_argument('--checkpoint', default='', type=str, help='A .tpg checkpoint to start from')
parser.add_argument('--team_size', default=20, type=int, help='The number of teams in the total population')
parser.add_argument('--root_size', default=0, type=int, help='The max number of root teams in the population. Zero means you do not care')
parser.add_argument('--epochs', default=5, type=int, help='The number of iterations of training')
parser.add_argument('--frames', default=500, type=int, help='The amount of time allocated to training per policy (in frames)')

args = parser.parse_args()
game_name = args.game
checkpoint = args.checkpoint
team_size = args.team_size
root_size = args.root_size
epochs = args.epochs
frames = args.frames

def main():
    gen = 0
    env = retro.make(game_name)
    env = SonicDiscretizer(env)
    tStart = time.time()
    trainer = None
    if checkpoint == '' or checkpoint == None:
        trainer = TpgTrainer(actions=range(7), teamPopSize=team_size, rTeamPopSize=root_size)
    else:
        checkpointLocation = os.path.join('.', 'checkpoints', args.checkpoint)
        if os.path.exists(checkpointLocation):
            gen = int(args.checkpoint[:-4].split('_')[1])
            with open(checkpointLocation, 'rb') as f:
                trainer = pickle.load(f)
        else:
            print("Something went wrong. Looked at {}".format(checkpointLocation))
            return -1

    curScores = []
    summaryScores = []
    while True:
        curScores = []
        gen += 1
        current_state = random.choice(SonicAndKnucklesState)
        print('Generation: {}; State: {}'.format(gen, current_state))
        env.load_state(current_state)
        while True:
            teamNum = trainer.remainingAgents()
            agent = trainer.getNextAgent()
            if agent is None:
                break
            
            if agent.taskDone():
                score = agent.getOutcome()
            else:
                state = env.reset()
                score = 0
                maxDistance = 0
                last_x = 0

                for i in range(frames):
                    '''show_state(env, i, 'Sonic3', 'Gen #' + str(gen) +
                            ', Team #' + str(teamNum) +
                            ', Score: ' + str(score))'''
                    
                    act = agent.act(getState(np.array(state, dtype=np.int32)))
                    if os.getenv('RENDER_AVAILABLE'):
                        env.render()
                    state, reward, isDone, debug = env.step(act)
                    score += abs(last_x - reward)
                    last_x = reward
                    if reward > maxDistance:
                        maxDistance = reward                
                    #score += reward

                    if isDone:
                        break
                adjustedScore = maxDistance * 100 + score
                #agent.reward(score)
                agent.reward(adjustedScore)
                if gen % int(os.getenv('LOG_GENERATION_GAP')) == 0:
                    print('Gen #' + str(gen) + ', Team #' + str(teamNum) + ', Score: ' + str(score) + ', Adjusted: ' + str(adjustedScore))
            
            curScores.append(score)

        summaryScores.append((min(curScores), max(curScores), sum(curScores) / len(curScores)))
        trainer.evolve()

        if gen % 5 == 0:
            with open('./checkpoints/checkpoint_' + str(gen) + '.tpg', 'wb') as f:
                pickle.dump(trainer, f)

    print('Time Taken (Seconds): ' + str(time.time() - tStart))
    print('Results:\nMin, Max, Avg')
    for result in summaryScores:
        print(result[0], result[1], result[2])


if __name__ == '__main__':
    main()
